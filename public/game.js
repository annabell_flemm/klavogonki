window.onload = () => {

    const jwt = localStorage.getItem('jwt');
    let words = [];
    let currentWord = 0;
    let Player = {}; //temporary solution

    if (!jwt) {
        location.replace('/login');
    } else {
        const socket = io.connect('http://localhost:3000');
        socket.emit('game-ready', {
            token: jwt
        });

        document.getElementById('game-text').addEventListener('keyup', event => {
            const {
                target: {
                    value
                }
            } = event;
            checkWord(getWords(value), words);

        });

        // socket.emit('game-ready', {
        //     token: jwt
        // });

        const checkWord = (userText, checkText) => {
            const gametext = document.getElementById('gametext');
            gametext.innerHTML =
                words.reduce((accumulator, currentValue, index) => {
                    accumulator += index === currentWord ? `<span class="current-word">${currentValue}</span>` : currentValue;
                    return accumulator + ' ';
                }, '').trim();
            if (currentWord >= checkText.length)
                return;
            if (userText.length > currentWord && userText[currentWord] === checkText[currentWord]) {
                gametext.innerHTML =
                    words.reduce((accumulator, currentValue, index) => {
                        if (index === currentWord) {
                            words[index] = `<span class="correct-word">${currentValue}</span>`;
                            accumulator += words[index];
                        } else {
                            accumulator += currentValue;
                        }
                        return accumulator + ' ';
                    }, '').trim();
                currentWord++;
                const progress = (currentWord + 1) * 100 / words.length;
                const player = document.getElementById(jwt);
                player.getElementsByTagName('progress')[0].setAttribute('value', progress);
                player.getElementsByTagName('strong')[0].innerHTML = `Progress ${progress}%`;
                checkWord(userText, checkText);
            }
        };

        const getWords = (text) => text ? text.trim().split(/\s+/) : [];

        const getWinner = () => {
            const max = Math.max.apply(
                Math,
                Array.from(document.querySelectorAll('.player-list .player progress'))
                    .map(item => item.getAttribute('value')));
            Array.from(document.querySelectorAll('.player-list .player'))
                .forEach(item => {
                    if (item.getElementsByTagName('progress')[0].getAttribute('value') == max) {
                        item.getElementsByTagName('h3')[0].innerHTML = `Winner ${item.getElementsByTagName('h3')[0].innerHTML} !`;
                    }
                });
        };

        socket.on('counter', data => {
            const jwt = localStorage.getItem('jwt');
            document.getElementById('timer').innerHTML = data.value;
            document.getElementsByClassName('timer')[0].style.display = 'block';
            if (data.value === 0) {
                socket.emit('counter', {
                    value: false,
                    token: jwt
                });
            }
        });

        const proxy = new Proxy(Player, {
            set(target, prop, value) {
                console.log(`Write ${prop} ${value}`);
                target[prop] = value;
                return true;
            }
        });

        socket.on('game', data => {
            const timer = document.getElementsByClassName('timer')[0];
            if (data.text) {
                timer.style.display = 'none';
                Array.from(document.getElementsByClassName('race'))
                    .forEach(el => el.style.display = 'block');
                document.getElementsByClassName('player-list')[0].innerHTML = data.gamers.map(item => `
                <li id="${item.token}" class="player">
                    <h3>${item.login}</h3>
                    <progress class="progress" max="100" value="0">
                    <strong>Progress: 0%</strong>
                    </progress>
                </li>`);
                document.getElementById('gametext').innerHTML = data.text;
                words = getWords(data.text);
                
                Player = { ...data.gamers.find(item => item.token === jwt) } ;
                console.log(Player);
            }
            document.querySelector('span.race').innerHTML = data.value;
            if (data.value === 0) {
                timer.style.display = "none";
                Array.from(document.querySelectorAll('p.race, input.race, span.race'))
                    .forEach(el => el.style.display = 'none');
                document.getElementById('gametext').innerHTML = '';
                document.getElementById('game-text').value = '';
                getWinner();
                setTimeout(() => {
                    socket.emit('counter', { value: true });
                }, 3000);
            }
        });

        socket.on('disconnect', data => {
            const item = document.getElementById(data.token);
            if (item) {
                item.remove();
            }
        });
    }
}