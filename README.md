# Klavogonki

This project was created for training purposes for [Binary Studio Academy](https://study.binary-studio.com/).
Based on game [Klavogonki](http://klavogonki.ru/).

# Built With

- [Node.js](https://nodejs.org) - JavaScript runtime powering the server
- [Express](http://expressjs.com/) - Node.js web framework
- [Socket.IO](https://socket.io/) - JavaScript library for realtime web application
- [PassportJS](http://www.passportjs.org/) - Passport is authentication middleware for Node.j


