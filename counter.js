const gametext = require('./gametext.json');

const timeToConnect = 5; // 10;
let _timeToConnect = timeToConnect;
const timeToGame = 1050; //
let _timeToGame = timeToGame;
let _timeStr;
let isStarted = false;
let interval;

class Counter {
    constructor(io, gamers) {
        this.io = io;
        this.gamers = gamers;
    }

    randomInteger(min, max) {
        let rand = min + Math.random() * (max + 1 - min);
        rand = Math.floor(rand);
        return rand;
    }

    getGameText() {
        const index = this.randomInteger(1, 3);
        return gametext.find(item => item.id == index).text;
    }

    start() {
        if (!isStarted) {
            interval = setInterval(() => {
                _timeStr = `${_timeToConnect} sec left to start...`;
                this.io.in('game-room').emit('counter', {
                    value: _timeToConnect,
                    message: _timeStr
                });
                if (!_timeToConnect) {
                    _timeToConnect = timeToConnect;
                } else {
                    _timeToConnect--;
                }
            }, 1000);
            isStarted = true;
        }
    }

    stop() {
        const text = this.getGameText();
        isStarted = false;
        clearInterval(interval);
        _timeToConnect = timeToConnect;

        interval = setInterval(() => {
            _timeStr = `${_timeToGame} sec left to end...`;
            if (_timeToGame === timeToGame) {
                // this.io.of('/').clients((err, clients) => {
                //     if (err) throw err;
                //     console.log(clients);
                // });
                this.io.in('game-room').emit('game', {
                    value: _timeToGame,
                    message: _timeStr,
                    text: text,
                    gamers: this.gamers
                });
            } else {
                this.io.in('game-room').emit('game', {
                    value: _timeToGame,
                    message: _timeStr
                });
            }

            if (!_timeToGame) {
                _timeToGame = timeToGame;
                clearInterval(interval);
                console.log('end');
            } else {
                _timeToGame--;
            }
        }, 1000);

    }
}

module.exports = Counter;