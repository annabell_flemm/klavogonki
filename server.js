const path = require('path');
const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const jwt = require('jsonwebtoken');
const passport = require('passport');
const bodyParser = require('body-parser');
const users = require('./users.json');
//const nsp = io.of('/game-nsp');

const Counter = require('./counter');
const { GirlPlayer, BoyPlayer, Player } = require('./player');
const gamers = [];
const counter = new Counter(io, gamers);

require('./passport.config');

server.listen(3000);

app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
app.use(bodyParser.json());

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.get('/game', function (req, res) {
  res.sendFile(path.join(__dirname, 'game.html'));
});

app.get('/login', function (req, res) {
  res.sendFile(path.join(__dirname, 'login.html'));
});

app.post('/login', function (req, res) {
  const userFromReq = req.body;
  const userInDB = users.find(user => user.login === userFromReq.login);
  if (userInDB && userInDB.password === userFromReq.password) {
    const token = jwt.sign(userFromReq, 'someSecret', {
      expiresIn: '24h'
    });
    res.status(200).json({
      auth: true,
      token
    });
  } else {
    res.status(401).json({
      auth: false
    });
  }
});

io.on('connection', socket => {
  let _token;
  socket.join('game-room');
  // socket.on('submitMessage', payload => {
  //   const {
  //     message,
  //     token
  //   } = payload;
  //   const userLogin = jwt.decode(token).login;
  // socket.broadcast.emit('newMessage', { message, user: userLogin });
  // socket.emit('newMessage', { message, user: userLogin });
  // io.in('game-room').emit('newMessage', {
  //   message,
  //   user: userLogin
  // });
  // });
  socket.on('game-ready', data => {
    const {
      token
    } = data;
    const decoded = jwt.verify(token, 'someSecret');
    _token = token;
    // gamers.push({
    //   login: decoded.login,
    //   token: _token
    // });
    gamers.push(new BoyPlayer(decoded.login, _token)); 
    counter.start();
  });

  socket.on('counter', data => {
    const {
      value
    } = data;
    if (value) {
      counter.start();
    } else {
      counter.stop();
    }
  });

  socket.on('disconnect', (reason) => {
    io.in('game-room').emit('disconnect', {
      token: _token
    });
    for (let i = 0; i < gamers.length; i++) {
      if (gamers[i].token === _token) {
        gamers.splice(i, 1);
      }
    }
  });
});

//const a = new Player('1', '11', 'Boy');
// console.log(a);

//const a = new BoyPlayer('login', '1'); 
//console.log(a.login);
// const a = new GirlPlayer('11', '2222');
// a.startedAt = new Date();
// a.finishedAt = new Date(2019,07,11);
// console.log(a.spentTime1()(), a.spentTime());